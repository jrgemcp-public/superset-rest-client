# superset-rest-client
A dumb pypi rest client for Apache Superset

### How to use

If you want to use this dumb pypi rest client, do the following

```
#!/bin/bash

pip3 install -e git+https://gitlab.com/jrgemcp-public/superset-rest-client.git@main#egg=superset-api-client --force-reinstall

echo DONE

```
