import requests
import json


def create_db_connection(base_url, session, bearer_token, csrf_token, db_ip, db_port, db_user, db_pass, db_name, conn_name):

    url = base_url + "/api/v1/database/"

    sqlalchemy_url = "postgresql://" + db_user + ":" + db_pass + "@" + db_ip + ":" + str(db_port)+"/" + db_name

    data_out = {
      "allow_csv_upload": True,
      "allow_ctas": False,
      "allow_cvas": False,
      "allow_dml": False,
      "allow_multi_schema_metadata_fetch": False,
      "allow_run_async": False,
      "cache_timeout": 0,
      "database_name": conn_name,
      "expose_in_sqllab": True,
      "impersonate_user": False,
      "sqlalchemy_uri": sqlalchemy_url
    }

    head = {
        "Authorization": "Bearer " + bearer_token,
        "X-CSRFToken": csrf_token
    }

    response = session.post(url=url, headers=head, json=data_out)
    response.raise_for_status()

    return response


def login_authenticate_createdb(superset_url,
                                superset_user,
                                superset_pass,
                                superset_conn_name,
                                db_user,
                                db_pass,
                                db_host,
                                db_port,
                                db_name):

    session = requests.session()

    login_url = superset_url + "/api/v1/security/login"

    login_data = {
        "password": superset_pass,
        "provider": "db",
        "refresh": False,
        "username": superset_user
    }

    response = session.post(url=login_url, json=login_data)
    response.raise_for_status()

    head = {
        "Authorization": "Bearer " + json.loads(response.text)['access_token']
    }
    csrf_url = superset_url + "/api/v1/security/csrf_token"

    csrf_response = session.get(csrf_url, headers=head)
    csrf_response.raise_for_status()

    create_response = create_db_connection(superset_url, session, bearer_token=json.loads(response.text)['access_token'],
                                           csrf_token=json.loads(csrf_response.text)['result'], db_ip=db_host,
                                           db_port=db_port, db_user=db_user, db_pass=db_pass, db_name=db_name,
                                           conn_name=superset_conn_name)

    print(str(create_response.text))